from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
from faker import Faker
import unittest, time, re, datetime
import os
import pickle
from dotenv import load_dotenv


class Scenariotest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r'C:\Users\admin\Documents\Onebrick\chromedriver.exe')
        self.driver.implicitly_wait(5)
        self.verificationErrors = []
        self.accept_next_alert = True



    def test_register(self):
        driver = self.driver
        #wait = WebDriverWait(driver, 20)
        fake = Faker()
        fake.email()
        emailFake = fake.email()
        for _ in range(1):
            driver.get("https://brick-qa-assignment.herokuapp.com/")
            driver.maximize_window()
            driver.find_element_by_id("firstName").click()
            driver.find_element_by_id("firstName").clear()
            driver.find_element_by_id("firstName").send_keys("test")
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'firstName')))
            driver.find_element_by_id("lastName").click()
            driver.find_element_by_id("lastName").clear()
            driver.find_element_by_id("lastName").send_keys("name")
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'lastName')))
            driver.find_element_by_id("email").click()
            driver.find_element_by_id("email").clear()
            driver.find_element_by_id("email").send_keys(emailFake)
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'email')))
            driver.find_element_by_id("phoneNumber").click()
            driver.find_element_by_id("phoneNumber").clear()
            driver.find_element_by_id("phoneNumber").send_keys("+628595685")
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'phoneNumber')))
            driver.find_element_by_id("address").click()
            driver.find_element_by_id("address").clear()
            driver.find_element_by_id("address").send_keys("jakarta")
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'address')))
            driver.find_element_by_id("password").click()
            driver.find_element_by_id("password").clear()
            driver.find_element_by_id("password").send_keys("pass123")
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'password')))
            driver.find_element_by_id("confirm_password").click()
            driver.find_element_by_id("confirm_password").clear()
            driver.find_element_by_id("confirm_password").send_keys("pass123")
            time.sleep(3)
            #wait.until(EC.element_to_be_clickable((By.ID, 'confirm_password')))
            driver.find_element_by_name("register").click()
            time.sleep(3)
            driver.find_element_by_xpath(
                "(.//*[normalize-space(text()) and normalize-space(.)='Check your email to confirm your registration'])[1]/following::button[1]").click()
            time.sleep(3)
            print("SUCCESS REGISTER")

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
