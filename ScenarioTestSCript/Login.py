from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
from faker import Faker
import unittest, time, re, datetime
import os
import pickle
from dotenv import load_dotenv


class Scenariotest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r'C:\Users\admin\Documents\Onebrick\chromedriver.exe')
        self.driver.implicitly_wait(5)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_register(self):
        driver = self.driver
        #wait = WebDriverWait(driver, 20)
        driver.get("https://app.jubelio.com/login")
        driver.maximize_window()
        driver.find_element_by_name("email").click()
        driver.find_element_by_name("email").clear()
        driver.find_element_by_name("email").send_keys("pendekar5000@gmail.com")
        time.sleep(3)
        #wait.until(EC.element_to_be_clickable((By.ID, 'your_email')))
        driver.find_element_by_name("password").click()
        driver.find_element_by_name("password").clear()
        driver.find_element_by_name("password").send_keys("pass@123")
        time.sleep(3)
        #wait.until(EC.element_to_be_clickable((By.ID, 'password')))
        driver.find_element_by_xpath("//button[@type='submit']").click()
        time.sleep(3)
        driver.save_screenshot(r'C:\Users\admin\Documents\jubelio\ScenarioTestSCript\Screenshoot\Login\login.png')
        print("Success Login")

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
